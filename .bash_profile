export PATH=~/.local/bin/:$PATH
#USER STUFF
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
# PS1="\[\033[01;32m\]\u@\h\[\033[00m\]: \[\033[01;34m\]\\w\[\033[00m\]\n> "
PS1="\[\033[01;32m\]\u@\h\[\033[00m\]: \[\033[01;34m\]\\w\[\033[00m\]\$(parse_git_branch)\n> "
# Prompt no color
# PS1="\u@\h: \W\n> "
# export EDITOR=vim
# export VISUAL="$EDITOR"

bind 'set show-all-if-ambiguous on'

# Eternal bash history.
# ---------------------
# Undocumented feature which sets the size to "unlimited".
# http://stackoverflow.com/questions/9457233/unlimited-bash-history
export HISTFILESIZE=
export HISTSIZE=
export HISTTIMEFORMAT="[%F %T] "
# Change the file location because certain bash sessions truncate .bash_history file upon close.
# http://superuser.com/questions/575479/bash-history-truncated-to-500-lines-on-each-login
export HISTFILE=~/.bash_eternal_history
# Force prompt to write history after every command.
# http://superuser.com/questions/20900/bash-history-loss
PROMPT_COMMAND="history -a; $PROMPT_COMMAND"


alias ls="ls --color=always"
alias codolor='LS_COLORS="ow=01;36;40" && export LS_COLORS'
# display and term
#export DISPLAY=localhost:0.0
#export DISPLAY=:0
#export TERM=screen-256color
export TERM=xterm-256color

alias ll="ls -Al"
# export EDITOR="emacsclient -t"
export EDITOR="nvim"
export ALTERNATE_EDITOR=""
# alias e="emacsclient -t"
alias n="nvim"
alias vim="nvim"

alias o="xdg-open"
alias tmux='TERM=xterm-256color tmux'
# alias cmd to cmd.exe
# alias cmd="cmd.exe /c"
# alias pwr="powershell.exe /c"
# alias wsu="cmd.exe /c powershell -Command 'Start-Process cmd -Verb RunAs'"
# Tracer
# alias tracer="ssh -R 52698:127.0.0.1:52698 saahil@tracer.hopto.org"

# alias code-insiders
# alias code="cmd.exe /c code"
# alias ff="cmd.exe /c firefox"
# alias ch="cmd.exe /c start chrome"

# alias for local ssh 
# alias sshBuntu="ssh -p 1111 smclaypool@localhost"
# alias sshAssembly="ssh -X -p 1112 saahil@localhost"
# alias sshAlg="ssh -X -R 52698:127.0.0.1:52698 olson_student@ace.wpi.edu"

#remapped aliases  
##cd -> cd + ls 
#cd() { builtin cd "$@" && ls;  }  
#ls -> ls + extensions alias 
ls="ls -p --color=auto"


function gig() { curl -L -s https://www.gitignore.io/api/$@ ; }

# Go stuff
# export GOROOT=$HOME/.local/go/
# export PATH=$PATH:$GOROOT/bin
# export PATH=$PATH:$HOME/go/bin

[ -f ~/.local.bash ] && source ~/.local.bash

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export FZF_CTRL_T_OPTS='--height 40% --layout=reverse --preview "head -100 {}"'


# --files: List files that would be searched but do not search
# --no-ignore: Do not respect .gitignore, etc...
# --hidden: Search hidden files and folders
# --follow: Follow symlinks
# --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)

if [ -x "$(command -v rg)" ]; then
    export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
fi

if [ -x "$(command -v bat)" ]; then
    export FZF_CTRL_T_OPTS="--ansi --preview-window 'right:60%' --preview 'bat --theme='OneHalfLight' --color=always --style=header,grid --line-range :300 {}'"

    export BAT_CONFIG_PATH="~/dotfiles/.bat.conf"
    alias bat="bat --theme='OneHalfLight'"
    alias cat=bat

fi

# mkdir -p ~/Software/; cd ~/Software; git clone git://github.com/wting/autojump.git 
# cd autojump; ./install.py
[[ -s /home/saahil/.autojump/etc/profile.d/autojump.sh  ]] && source /home/saahil/.autojump/etc/profile.d/autojump.sh


export PATH="$HOME/.cargo/bin:$PATH"

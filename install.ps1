# @"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
choco install -y --ignorechecksum python
choco install -y --ignorechecksum conemu 
choco install -y --ignorechecksum autohotkey.install 
choco install -y --ignorechecksum dotnetcore-sdk 
choco install -y --ignorechecksum visualstudiocode 
choco install -y --ignorechecksum git.install 
choco install -y --ignorechecksum steam 
choco install -y --ignorechecksum leagueoflegends 
choco install -y --ignorechecksum kindle
choco install -y --ignorechecksum discord.install 
choco install -y --ignorechecksum googledrive 
choco install -y --ignorechecksum googlechrome
choco install -y --ignorechecksum virtualbox 
choco install -y --ignorechecksum vagrant 
choco install -y --ignorechecksum neovim # needed for vscode
choco install -y --ignorechecksum jdk8 
choco install -y --ignorechecksum nodejs.install
choco install -y --ignorechecksum rainmeter 
choco install -y --ignorechecksum pandoc 
choco install -y --ignorechecksum miktex 
choco install -y --ignorechecksum wireshark 
choco install -y --ignorechecksum vcxsrv 
choco install -y --ignorechecksum sharpkeys 
choco install -y --ignorechecksum zeal.install 
choco install -y --ignorechecksum fzf
choco install -y firacode
cp profile.ps1 ~/Documents/WindowsPowershell/
cp .vimrc ~
mkdir ~/AppData/Local/nvim/
cp init.vim ~/AppData/Local/nvim/init.vim
md ~\.vim\autoload
$uri = 'https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
(New-Object Net.WebClient).DownloadFile(   
    $uri,
    $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath(     
        "~\.vim\autoload\plug.vim"
        )
)
Install-Module posh-git
Install-Module PSFzf
# Battle.net
# visual studio
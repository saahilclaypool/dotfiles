// an example to create a new mapping `ctrl-y`
api.mapkey('<ctrl-y>', 'Show me the money', function() {
    Front.showPopup('a well-known phrase uttered by characters in the 1996 film Jerry Maguire (Escape to close).');
});

// an example to replace `T` with `gt`, click `Default mappings` to see how `T` works.
api.map('gt', 'T');
api.map('H', 'S')
api.map('L', 'D')
api.unmap('<ctrl-alt-i>')

// an example to remove mapkey `Ctrl-i`
// api.unmap('<ctrl-i>');

settings.scrollStepSize = 140;

settings.theme = `
    #sk_status, #sk_find, #sk_omnibarSearchArea>input, #sk_omnibarSearchResult {
        font-size: 16pt !important;
    }
}`;
settings.hintAlign = "left";
api.Hints.setCharacters('fdstgvear'); 
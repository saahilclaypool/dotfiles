# sudo apt install i3 rofi compton wmctrl
mkdir -p ~/.config/plasma-workspace/env/
echo "export KDEWM=/usr/bin/i3" > ~/.config/plasma-workspace/env/set_window_manager.sh
cp -rf ~/dotfiles/i3 ~/.config
cp ~/dotfiles/i3/.Xresources ~
cp ~/dotfiles/i3/.xinitrc ~

# toggle off with 
# echo "export KDEWM=kwin" > ~/.config/plasma-workspace/env/set_window_manager.sh


" set termguicolors
"----------------------------------------------------------------------- PLUG

"curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"

""
" START plugins
call plug#begin('~/.vim/plugged')
" UI -----------------------------------------------
" Plug 'psliwka/vim-smoothie'
" let g:smoothie_base_speed = 7
" let g:smoothie_break_on_reverse = 0
" Plug 'RRethy/vim-illuminate'
"" hi link IlluminatedWord MatchParen

Plug 'christoomey/vim-tmux-navigator'
let g:tmux_navigator_no_mappings = 1
" Disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

nnoremap <silent> <M-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <M-j> :TmuxNavigateDown<cr>
nnoremap <silent> <M-k> :TmuxNavigateUp<cr>
nnoremap <silent> <M-l> :TmuxNavigateRight<cr>

inoremap <silent> <M-h> <c-o>:TmuxNavigateLeft<cr>
inoremap <silent> <M-j> <c-o>:TmuxNavigateDown<cr>
inoremap <silent> <M-k> <c-o>:TmuxNavigateUp<cr>
inoremap <silent> <M-l> <c-o>:TmuxNavigateRight<cr>

tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateLeft<cr>
tnoremap <silent> <M-j> <C-\><C-N>:TmuxNavigateDown<cr>
tnoremap <silent> <M-k> <C-\><C-N>:TmuxNavigateUp<cr>
tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateRight<cr>

Plug 'junegunn/seoul256.vim'
Plug 'NLKNguyen/papercolor-theme'
Plug 'NLKNguyen/c-syntax.vim'
Plug 'lifepillar/vim-solarized8'
Plug 'junegunn/goyo.vim'
let g:goyo_width = '120' 
let g:goyo_height = '100%'
let g:goyo_linenr = 0
nnoremap <silent> <C-w>z :Goyo 100%x100%<cr>
nnoremap <silent> <C-w><C-z> :Goyo!<cr>

Plug 'vim-airline/vim-airline'
let g:airline_extensions = ['branch', 'tabline']
Plug 'vim-airline/vim-airline-themes'
let g:airline#extensions#tabline#enabled = 0
let g:airline_detect_spell=1
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#branch#format = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
nmap <leader>1 <Plug>AirlineSelectTab1
nmap <leader>2 <Plug>AirlineSelectTab2
nmap <leader>3 <Plug>AirlineSelectTab3
nmap <leader>4 <Plug>AirlineSelectTab4
nmap <leader>5 <Plug>AirlineSelectTab5
nmap <leader>6 <Plug>AirlineSelectTab6
nmap <leader>7 <Plug>AirlineSelectTab7
nmap <leader>8 <Plug>AirlineSelectTab8
nmap <leader>9 <Plug>AirlineSelectTab9
nmap <leader>- <Plug>AirlineSelectPrevTab
nmap <leader>= <Plug>AirlineSelectNextTab

"
Plug 'markonm/traces.vim'
set signcolumn=yes
Plug 'Shougo/echodoc.vim'
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'echo'

Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
map <c-\> <plug>NERDTreeMirrorToggle<CR>
map <leader><c-\> :NERDTreeFind<CR>
let g:NERDTreeWinPos = 'right'
let g:NERDTreeHijackNetrw=0

" GIT ====================================================
Plug 'tpope/vim-fugitive'
nnoremap <leader>gg :Gstatus<CR>
nnoremap <leader>gl :Glog!<CR>
nnoremap <leader>gd :Gvdiff<Space>
nnoremap <leader>GD :Gdiffsplit!<CR>
nnoremap <leader>gb :Gblame<CR>
nnoremap <leader>ge :Gvsplit origin/develop:%<Space>

Plug 'airblade/vim-gitgutter'
let g:gitgutter_map_keys = 0
nmap ]g <Plug>(GitGutterNextHunk)
nmap [g <Plug>(GitGutterPrevHunk)
nnoremap <leader>gs :GitGutterStageHunk<CR>
nnoremap <leader>gu :GitGutterUndoHunk<CR>
" TOGGLE --------------------------------------------
nnoremap <silent> gp :call ToggleGitGutterPreviewHunk()<CR>
function! ToggleGitGutterPreviewHunk() abort
    " does nothing if that command doesn't exist
    if !exists(':GitGutterPreviewHunk')
        return 0
    endif

    " loop through all the windows in the current tab page
    for win in range(1, winnr('$'))

        " is it a preview window?
        let preview_window = getwinvar(win, '&previewwindow') ? win : 0
    endfor

    " we have a preview window
    if preview_window > 0

        " close the preview window
        pclose

        " we don't have a preview window
    else

        " open the preview window
        GitGutterPreviewHunk
    endif
endfunction
" --------------------------------------------------
" Terminal --------------------------------------------------
Plug 'kassio/neoterm'
xmap <Leader>, <Plug>(neoterm-repl-send)
nmap <Leader>, <Plug>(neoterm-repl-send-line)
let g:neoterm_size = 10
let g:neoterm_autoinsert = 1
let g:neoterm_default_mod = 'rightbelow'
map <silent> <C-space> :Ttoggle<cr>
map <silent> <leader><C-space> :terminal<cr>i

Plug 'tpope/vim-dispatch'

" VIM basics ----------------------------------------------
Plug 'junegunn/vim-peekaboo'
let g:peekaboo_delay = 550
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
let g:NERDAltDelims_c = 1
map <silent> \ <leader>c<leader>
map <silent> <leader>/ <leader>c<leader>
let g:NERDCustomDelimiters = {
            \ 'test': { 'left': '//'},
            \ }

Plug 'junegunn/vim-easy-align'
vnoremap <silent> a :LiveEasyAlign<CR>

Plug 'justinmk/vim-sneak'
"map f <Plug>Sneak_f
"map F <Plug>Sneak_F
"map t <Plug>Sneak_t
"map T <Plug>Sneak_T
let g:sneak#label = 1
let g:sneak#use_ic_scs = 1
  nnoremap <silent> f :<C-U>call sneak#wrap('',           1, 0, 1, 1)<CR>
  nnoremap <silent> F :<C-U>call sneak#wrap('',           1, 1, 1, 1)<CR>
  xnoremap <silent> f :<C-U>call sneak#wrap(visualmode(), 1, 0, 1, 1)<CR>
  xnoremap <silent> F :<C-U>call sneak#wrap(visualmode(), 1, 1, 1, 1)<CR>
  onoremap <silent> f :<C-U>call sneak#wrap(v:operator,   1, 0, 1, 1)<CR>
  onoremap <silent> F :<C-U>call sneak#wrap(v:operator,   1, 1, 1, 1)<CR>

Plug 'michaeljsmith/vim-indent-object'
Plug 'mbbill/undotree'
map <Leader>u :UndotreeToggle<cr>:UndotreeFocus<cr>
Plug 'raimondi/delimitMate'
let delimitMate_expand_cr = 2
let g:delimitMate_nesting_quotes = ['"','`']
"Plug 'machakann/vim-highlightedyank'
"let g:highlightedyank_highlight_duration = 500
Plug 'andymass/vim-matchup'
let g:matchup_matchparen_status_offscreen = 0
let g:matchup_matchparen_offscreen = {'method': 'float', 'scrolloff': 1}

" Search --------------------------------------------------
Plug 'mileszs/ack.vim'
Plug 'yssl/QFEnter' " make quickfix open in last window
" align the quickfix window
Plug 'MarcWeber/vim-addon-qf-layout'
let g:vim_addon_qf_layout = {}
"let g:vim_addon_qf_layout.quickfix_formatters = [ 'vim_addon_qf_layout#DefaultFormatter', 'NOP', 'vim_addon_qf_layout#FormatterNoFilename', 'vim_addon_qf_layout#Reset' ]
let g:vim_addon_qf_layout.quickfix_formatters = [ 'vim_addon_qf_layout#DefaultFormatter', 'vim_addon_qf_layout#Reset']
let g:vim_addon_qf_layout.lhs_cycle = '<buffer> <space>ff'
let g:vim_addon_qf_layout.file_name_align_max_width = 30
if executable('rg')
    let g:ackprg = 'rg --no-heading --vimgrep --smart-case'
endif

nnoremap <Leader>a :Ack!<Space>
vnoremap <Leader>a ygv:<c-u>Ack!<Space><C-R>"<Space>
nnoremap <Leader>A :Ack!<Space><C-r>/<Space><C-r>%<Space>

Plug 'airblade/vim-rooter'
let g:rooter_patterns = ['Cargo.toml', 'Rakefile', '.git/', '.root']
let g:rooter_manual_only = 0
nnoremap <Leader>CD :Rooter<CR>:source .vimrc<CR>:pwd<CR>

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

nnoremap <C-p> :FZF<CR>
nnoremap <Leader>b :Buffers<CR>
nnoremap <leader><tab> :Buffers<CR>
nnoremap <Leader>s :BLines<CR>
nnoremap <Leader>S :Rg<space>
nnoremap <Leader><Leader>s :Lines<CR>
nnoremap <Leader>H :Helptags<CR>
nnoremap <Leader>h :History<CR>
nnoremap <leader>;  :<up>
let g:fzf_buffers_jump = 0
let g:fzf_tags_command = 'ctags -R --sort=yes --excmd=number --tag-relative=yes --machinable=yes'
nnoremap <silent> <Leader>t :BTags<CR>
nnoremap <silent> <Leader>T :Tags<CR>
nnoremap <Leader>i :ToggleIgnore<CR>

let g:USE_IGNORE = 1
let g:_FZF_DEFAULT_COMMAND = "rg --files --hidden --glob \"!.git/*\" --no-messages "


function! ToggleIgnore()
    if g:USE_IGNORE == 1
        let $FZF_DEFAULT_COMMAND.='--no-ignore '
        let g:USE_IGNORE = 0
        echo "FZF ignore OFF"
    else
        let $FZF_DEFAULT_COMMAND = g:_FZF_DEFAULT_COMMAND
        let g:USE_IGNORE = 1
        echo "FZF ignore ON"
    endif
endfunction
command! -complete=command ToggleIgnore call ToggleIgnore()
if !has("win32")
    call ToggleIgnore()
endif

if has("nvim")
    au TermOpen * tnoremap <Esc> <c-\><c-n>
    au TermOpen * tnoremap <c-w><Esc><Esc> <Esc>
    au TermOpen * tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateLeft<cr>
    au TermOpen * tnoremap <silent> <M-j> <C-\><C-N>:TmuxNavigateDown<cr>
    au TermOpen * tnoremap <silent> <M-k> <C-\><C-N>:TmuxNavigateUp<cr>
    au TermOpen * tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateRight<cr>
    au BufEnter * if &buftype == 'terminal' | :startinsert | endif
    autocmd! FileType fzf
    "autocmd  FileType fzf set laststatus=0 noshowmode noruler
    "\| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
    au FileType fzf tunmap <Esc>
    " FZF in a floating window
    "let $FZF_DEFAULT_OPTS.='--layout=reverse'
    let g:fzf_layout = { 'window': 'call FloatingFZF()' }

    function! FloatingFZF()
        let buf = nvim_create_buf(v:false, v:true)
        call setbufvar(buf, '&signcolumn', 'no')

        "let height = &lines - 5
        let height = &lines / 2
        let width = min([(&columns * 7 / 10), 120])

        "let col = float2nr((&columns - width) / 2)
        let col = float2nr(0)
        let row = float2nr(&lines / 2)
        "let row = 5

                    "\ 'row': &lines / 2,
                    "\ 'col': &columns - &width / 2,
                    "\ 'relative': 'win',
        let opts = {
                    \ 'relative': 'win',
                    \ 'row': row,
                    \ 'col': col,
                    \ 'width': width,
                    \ 'height': height,
                    \ 'style': 'minimal'
                    \ }

        call nvim_open_win(buf, v:true, opts)
    endfunction
endif

" When using `dd` in the quickfix list, remove the item from the quickfix list.
" cdo will apply to all lines in the quickfix window
function! RemoveQFItem()
    let curqfidx = line('.') - 1
    let qfall = getqflist()
    call remove(qfall, curqfidx)
    call setqflist(qfall, 'r')
    execute curqfidx + 1 . "cfirst"
    :copen
endfunction
:command! RemoveQFItem :call RemoveQFItem()
" Use map <buffer> to only map dd in the quickfix window. Requires +localmap
autocmd FileType qf map <buffer> dd :RemoveQFItem<cr>

" IDE --------------------------------------------------

"Plug 'mhinz/vim-startify'
"let g:startify_session_delete_buffers = 1
"let g:ascii = ['VIM']
"let g:startify_lists = [
            "\ { 'type': 'sessions',  'header': ['   Sessions']       },
            "\ { 'type': 'dir',       'header': ['   MRU '. getcwd()] },
            "\ { 'type': 'files',     'header': ['   MRU']            },
            "\ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
            "\ { 'type': 'commands',  'header': ['   Commands']       },
            "\ ]
"let g:startify_custom_header = g:ascii
"Plug 'chrisbra/csv.vim'
"let g:no_csv_maps = 1
Plug 'sheerun/vim-polyglot'
let g:jsx_ext_required = 1
let g:polyglot_disabled = ['latex', 'tex', 'markdown', 'csv']
let g:tex_flavor = "latex"
Plug 'lervag/vimtex'
Plug 'jceb/vim-orgmode'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

let g:vim_markdown_override_foldtext = 0
let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_folding_level = 10
let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_folding_style_pythonic = 1


" Completion--------------------------------------------------

set complete-=i
set pumheight=7

Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
let g:UltiSnipsSnippetDir="~/dotfiles/snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/snips']
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsListSnippets = "<c-y>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
let g:UltiSnipsEditSplit="vertical"

"source ~/dotfiles/coc_config.vim
"source ~/dotfiles/deoplete.vim
"source ~/dotfiles/languageclient.vim
"source ~/dotfiles/ncm2.vim

" install universal ctags
" Plug 'ludovicchabant/vim-gutentags'
Plug 'liuchengxu/vista.vim'
let g:vista_sidebar_width=40
let g:vista#renderer#enable_icon=0
let g:vista_echo_cursor_strategy="both"


nnoremap <silent> <leader>vv :Vista focus<cr>
nnoremap <silent> <leader>vc :Vista!!<cr>
nnoremap <silent> <leader>vf :Vista finder<cr>
nnoremap <silent> <leader>vg :Tags<cr>


" --------------------------------------------------

call plug#end()

if exists("deo")
    call deoplete#custom#source('ultisnips', 'rank', 1000)
    call deoplete#custom#var('omni', 'input_patterns', {
                \ 'tex': g:vimtex#re#deoplete
                \})
endif



" THEME --------------------------------------------------

"   Range:   252 (darkest) ~ 256 (lightest)
"let g:seoul256_background = 256
"colorscheme seoul256-light
set background=light " for the dark version

function! AirlineFilename()
    let name = expand("%")
    let name = ""
    let subs = split(fnamemodify(expand("%:h"), ":~:."), "/")
    let i = 1
    for s in subs
        let parent = name
        if  i == len(subs)
            let name = parent . '/' . s
        elseif i == 1
            let name = s
        else
            let name = parent . '/' . strpart(s, 0, 2)
        endif
        let i += 1
    endfor
    return name
endfunction



let g:PaperColor_Theme_Options = {
            \   'language': {
            \     'c': {
            \       'highlight_builtins' : 1
            \     },
            \    },
            \   'theme': {
            \     'default.light': {
            \       'transparent_background': 1,
            \       'allow_bold': 1,
            \       'allow_italic': 1,
            \       'override' : {
            \          'color00' : ['#fdf6e3', '256'],
            \          'color07' : ['#000000', '256'],
            \          'cursorline': ['#fdf6e3', '255'],
            \          'cursor_fg': ['#cc0000', '255'],
            \          'cursor_bg': ['#ffffff', '255'],
            \       }
            \     }
            \   }
            \ }
            "\          'color00' : ['#edf5ed', '256'],
            "\          'cursorline': ['#edf5ed', '254'],

let g:airline_section_a =     airline#section#create(['mode', 'paste', 'readonly'])
let g:airline_section_b =     airline#section#create([''])
let g:airline_section_c =     airline#section#create(['file'])
let g:airline_section_y =     airline#section#create(['filetype'])
let g:airline_section_x =     airline#section#create(['%-0.30{fnamemodify(getcwd(), ":~:.")}', '  %{airline#util#wrap(airline#extensions#branch#get_head(), 80)}'])
let g:airline_section_z = airline#section#create(['%5l', '/%-5L%', '::%3v', '  %3p%%'])
let g:airline_section_error = airline#section#create([''])
let g:airline_section_warning = airline#section#create([''])
let g:airline_theme='sol'
"let g:airline_theme='seagull'
let g:airline_mode_map = {
            \ '__'     : '-',
            \ 'c'      : 'C',
            \ 'i'      : 'I',
            \ 'ic'     : 'I',
            \ 'ix'     : 'I',
            \ 'n'      : 'N',
            \ 'multi'  : 'M',
            \ 'ni'     : 'N',
            \ 'no'     : 'N',
            \ 'R'      : 'R',
            \ 'Rv'     : 'R',
            \ 's'      : 'S',
            \ 'S'      : 'S',
            \ ''     : 'S',
            \ 't'      : 'T',
            \ 'v'      : 'V',
            \ 'V'      : 'V',
            \ ''     : 'V',
            \ }


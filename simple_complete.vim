Plug 'lifepillar/vim-mucomplete'
let g:mucomplete#enable_auto_at_startup = 1
set completeopt+=noselect,menuone

" Define default completion chain
let g:mucomplete#chains = { 'default':
    \ [ 'ulti','omni','keyn','keyp','tags','path','line'] }

Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
let g:UltiSnipsSnippetDir="~/dotfiles/snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/snips']
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsListSnippets = "<c-y>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
let g:UltiSnipsEditSplit="vertical"
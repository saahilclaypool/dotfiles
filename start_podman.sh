sudo service ssh start
sudo chmod 666 /dev/kvm && podman machine start

sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

systemctl --user enable podman.socket

echo 'run: podman system connection add wsl --identity C:\\Users\\saahi\\.ssh\\id_ed_localhost ssh://saahil@$((wsl hostname -I).Trim())/mnt/wslg/runtime-dir/podman/podman.sock'

podman system service --time=0 unix:///mnt/wslg/runtime-dir/podman/podman.sock

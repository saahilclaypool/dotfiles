$ahk_compiler = "C:\Program Files\AutoHotkey\Compiler\Ahk2Exe.exe"
$startup_dir = "C:\Users\saahi\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup"

Get-ChildItem | Where-Object Extension -eq ".exe" | ForEach-Object {Remove-Item $_.Name}
Get-ChildItem $startup_dir | Where-Object { $_.Name -like "*winmove*" -or $_.Name -like "*hjkl*" } | ForEach-Object {Remove-Item $_.Name}

Start-Sleep 2

$items = Get-ChildItem | Where-Object Extension -eq ".ahk"

$items | ForEach-Object {&$ahk_compiler /in $_.Name; Start-Sleep 1}

# $exes = Get-ChildItem | Where-Object Extension -eq ".exe" | ForEach-Object {Copy-Item $_.Name $startup_dir -Force}

# explorer.exe $startup_dir
>!h::
SendInput { left }
return

>!j::
SendInput { Down }
return

>!k::
SendInput { Up }
return

>!l::
SendInput { Right }
return

>!i::
SendInput { Home }
return

>!n::
SendInput { End }
return


+>!h::
SendInput +{ left }
return

+>!j::
SendInput +{ Down }
return

+>!k::
SendInput +{ Up }
return

+>!l::
SendInput +{ Right }
return

+>!i::
SendInput +{ Home }
return

+>!n::
SendInput +{ End }
return

^+>!h::
SendInput ^+{ left }
return

^+>!j::
SendInput ^+{ Down }
return

^+>!k::
SendInput ^+{ Up }
return

^+>!l::
SendInput ^+{ Right }
return

^+>!i::
SendInput ^+{ Home }
return

^+>!n::
SendInput ^+{ End }
return

^>!h::
SendInput ^{ left }
return

^>!j::
SendInput ^{ Down }
return

^>!k::
SendInput ^{ Up }
return

^>!l::
SendInput ^{ Right }
return

^>!i::
SendInput ^{ Home }
return

^>!n::
SendInput ^{ End }
return

>!o::
SendInput { PgUp }
return

>!u::
SendInput { PgDn }
return

>!'::
SendInput { Delete }
return


>!;::
SendInput { Delete }
return

$Capslock::Ctrl

#]::^#Right ; switch to next desktop with Windows key + Left Alt key
#[::^#Left ; switch to next desktop with Windows key + Left CTRL key

#if MouseIsOver("ahk_class Shell_TrayWnd")
    WheelDown::^#Right ; switch to next desktop with Windows key + Left Alt key
    WheelUp::^#Left ; switch to next desktop with Windows key + Left CTRL key
    MButton::#Tab ; switch to next desktop with Windows key + Left CTRL key
#if

>^Up::
    SendInput #{ t }
    return

>!w::AltTab
>!e::ShiftAltTab

MouseIsOver(WinTitle) {
    MouseGetPos,,, Win
    return WinExist(WinTitle . " ahk_id " . Win)
}

; >!Esc::
; SendInput { `` }
; return

; >!+Esc::
; SendInput { ~ }
; return

; sound

>!s::
Send {Volume_Down}
return

>!d::
Send {Volume_Up}
return

>!a::
SendInput {Media_Prev}
return

>!f::
SendInput {Media_Next}
return

>!g::
SendInput {Media_Play_Pause}
return

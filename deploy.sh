#!/bin/bash 

for host in $@
do
  echo "copying bashrc and vimrc to '$host'"
  scp ./base_bashrc $host:~/.bashrc
  scp ./.vimrc $host:~/.vimrc
  scp ./.tmux.conf $host:~/.tmux.conf
done

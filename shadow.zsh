alias tmux="tmux -f ~/.tmux.conf"
alias hist="history -E"

[ -d ~/.rbenv/bin ] && export PATH="$HOME/.rbenv/bin:$PATH"
[ -d ~/.rbenv/bin ] && eval "$(rbenv init -)"

function rbver {
 export RB_VERSION=$(grep "ruby " Gemfile | awk '{print $2}' | sed -e "s/'//g")
 echo $RB_VERSION
}



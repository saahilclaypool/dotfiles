"Guifont! Iosevka Term SS04 Medium:h12
set termguicolors
set guifont=Iosevka:h13
"call GuiWindowMaximized(1)

  "\          'color00' : ['#e5eae5', '256'],
let g:PaperColor_Theme_Options = {
  \   'theme': {
  \     'default.light': {
  \       'transparent_background': 0,
  \       'allow_bold': 1,
  \       'allow_italic': 1,
  \       'override' : {
  \          'color00' : ['#fdf6e3', '256'],
  \          'color07' : ['#000000', '256'],
  \          'cursorline': ['#ede6d3', '256'],
  \          'cursor_fg': ['0xcc0000', '256'],
  \          'cursor_bg': ['0xffffff', '256'],
  \       }
  \     }
  \   }
  \ }

tnoremap <s-space> <space>

GuiTabline 0
GuiPopupmenu 0

try
  colorscheme PaperColor
  catch
  try
    catch
  endtry
endtry

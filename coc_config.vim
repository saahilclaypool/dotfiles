Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
let g:UltiSnipsSnippetDir="~/dotfiles/snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/snips']
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsListSnippets = "<c-y>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
let g:UltiSnipsEditSplit="vertical"

Plug 'neoclide/coc.nvim', {'do': { -> coc#util#install()}}
let g:coc_global_extensions = [ "coc-json", 
            \ "coc-python", 
            \ "coc-lists", 
            \ "coc-ultisnips", 
            \ "coc-snippets", 
            \ "coc-rust-analyzer", 
            \ "coc-vimtex", 
            \ "coc-tsserver", 
            \ "coc-html", 
            \ "coc-emmet", 
            \] 
            "\ "coc-yank", 
" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300

inoremap <silent><expr> <TAB>
      \ pumvisible() ? coc#_select_confirm() :
      \ coc#expandableOrJumpable() ? coc#rpc#request('doKeymap', ['snippets-expand-jump','']) :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

let g:coc_snippet_next = '<Tab>'
let g:coc_snippet_prev = '<S-Tab>'
"let g:coc_node_path = "/usr/bin/node"

nnoremap <silent> <space>y  :<C-u>CocList --normal yank<cr>
inoremap <silent><expr> <c-space> coc#refresh()
" Use `[c` and `]c` for navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)
" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
nmap <leader><S-F>  <Plug>(coc-format)
command! -nargs=0 Format :call CocAction('format')

" Using CocList
"nnoremap <silent> <leader>;  :<C-u>CocList<cr>
nnoremap <silent> <leader>ll  :<C-u>CocList diagnostics<cr>
nnoremap <silent> <leader>le  :<C-u>CocList extensions<cr>
nnoremap <silent> <leader>lc  :<C-u>CocList commands<cr>
nnoremap <silent> <leader>lo  :<C-u>CocList outline<cr>
nnoremap <silent> <leader>ls  :<C-u>CocList -I symbols<cr>
nnoremap <silent> <leader>lr  <Plug>(coc-rename)
nnoremap <silent> <leader>j  :<C-u>CocNext<CR>
nnoremap <silent> <leader>k  :<C-u>CocPrev<CR>
nnoremap <silent> <leader>p  :<C-u>CocListResume<CR>

nmap <leader>gc  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>?  <Plug>(coc-fix-current)

" Use gh for show documentation in preview window
nnoremap <silent> gh :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call CocActionAsync('doHover')
  endif
endfunction

" ------------------------------------- ALE
source ~/dotfiles/ale.vim

echo "ssh to: $1"

ssh $1 mkdir -p ~/.vim/swapfiles
ssh $1 mkdir -p ~/.vim/undo
ssh $1 mv ~/.vimrc ~/.vimrc_old
scp ~/dotfiles/.vimrc $1:~/.vimrc
ssh $1 mv ~/.bashrc ~/.bashrc_old
scp ~/dotfiles/base_bashrc $1:~/.bashrc

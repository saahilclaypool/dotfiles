if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1
Plug 'ncm2/float-preview.nvim'

inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ deoplete#manual_complete()
function! s:check_back_space() abort "{{{
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" ---------------------------------------------- snippets
Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
let g:UltiSnipsSnippetDir="~/dotfiles/snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/snips']
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsListSnippets = "<c-y>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
let g:UltiSnipsEditSplit="vertical"


" ---------------------------------------------- IDE
source ~/dotfiles/ale.vim
" ---------------------------------------------- post update
function DeopleteOptions()
    call deoplete#custom#source('_', 'matchers', ['matcher_full_fuzzy'])
    call deoplete#custom#source('ultisnips', 'rank', 1000)
    call deoplete#custom#option('sources', {
                \ '_': ['around', 'buffer', 'file', 'member'],
                \})
endfunction

function! s:on_load(name, exec)
    if has_key(g:plugs[a:name], 'on') || has_key(g:plugs[a:name], 'for')
        execute 'autocmd! User' a:name a:exec
    else
        execute 'autocmd VimEnter *' a:exec
    endif
endfunction

call s:on_load('deoplete.nvim', 'call DeopleteOptions()')

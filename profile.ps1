# Add . ~/dotfiles/profile.ps1 into real profile
$env:Path = $env:Path + ";" + [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User")
$env:Path = "$env:UserProfile\AppData\Roaming\nvm;$env:Path"
$env:Path = "$env:Path;$env:userprofile\bin\Neovim\bin"
$env:Path = "C:\tools\miniconda3\;$env:Path"
$env:Path = "C:\tools\miniconda3\Scripts\;$env:Path"
$env:Path = "C:\tools\miniconda3\Library\bin\;$env:Path"
$env:Path = "C:\Program Files\R\R-4.1.2\bin;$env:Path"
$env:Path = "$env:UserProfile\AppData\Roaming\Python\Python39\Scripts;$env:Path"
$env:Path = "$env:Path;$env:USERPROFILE\.dotnet\tools"
remove-alias r
$env:Path = "$env:Path;C:\Program Files\nodejs\"
$env:Path = "$env:Path;C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE"
$env:Path = "$env:Path;C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\Extensions\TestPlatform"
$env:Path = "$env:Path;C:\Program Files\Git\usr\bin\"
$env:Path = "$env:userprofile\bin;$env:Path"

if (Get-Command "zoxide" -ErrorAction SilentlyContinue) {
  Invoke-Expression (& {
      $hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
      (zoxide init --hook $hook powershell) -join "`n"
    })
  set-alias -Name j -Value z
}

# Import-Module posh-git
$LazyLoadProfile = [PowerShell]::Create()
[void]$LazyLoadProfile.AddScript(@'
    Import-Module posh-git
'@)
[void]$LazyLoadProfile.BeginInvoke()

$null = Register-ObjectEvent -InputObject $LazyLoadProfile -EventName InvocationStateChanged -Action {
  Import-Module posh-git
  $LazyLoadProfile.Dispose()
}
import-module psreadline
Set-PSReadlineOption -EditMode Emacs
Set-PSReadLineKeyHandler -Key 'Ctrl+Backspace' -Function BackwardDeleteWord
Set-PSReadLineKeyHandler -Key 'Ctrl+w' -Function BackwardDeleteWord
Set-PSReadLineKeyHandler -Key 'shift+tab' -Function MenuComplete
# install-module psfzf
Import-Module PSFzf
Set-PsFzfOption -PSReadlineChordProvider 'Ctrl+t' -PSReadlineChordReverseHistory 'Ctrl+r'
$env:FZF_DEFAULT_COMMAND = 'rg --files --hidden --no-require-git'

# Set-PSReadLineKeyHandler -Key 'shift+tab' -ScriptBlock { Invoke-FzfTabCompletion }
$hostname = hostname
function prompt { "($hostname) $(pwd)`r`n❯ " }

if (Get-Command "starship" -ErrorAction SilentlyContinue) {
  Invoke-Expression (&starship init powershell)
}

# Import-Module posh-git
# $GitPromptSettings.DefaultPromptSuffix = '`n$(''>'' * ($nestedPromptLevel + 1)) '


#For PowerShell v3
Function gig {
  param(
    [Parameter(Mandatory = $true)]
    [string[]]$list
  )
  $params = ($list | ForEach-Object { [uri]::EscapeDataString($_) }) -join ","
  Invoke-WebRequest -Uri "https://www.toptal.com/developers/gitignore/api/$params" | select -ExpandProperty content | Out-File -FilePath $(Join-Path -path $pwd -ChildPath ".gitignore") -Encoding ascii
}

Set-Alias  -Name "c" -Value "code.cmd"
Set-Alias -Name "n" -Value "nvim"

function pandocd {
  param (
    [string] $input_filename
  )
  $filename = [System.IO.Path]::GetFileNameWithoutExtension($input_filename)
  $output = $filename + ".docx"
  Write-Host "converting" $input_filename "to" $output
  pandoc.exe -o $output $input_filename --standalone --self-contained -V fontsize=11pt -V geometry:"margin=0.75in" --variable urlcolor=blue --from markdown+implicit_figures
}

Set-Alias pd pandocd

function wsudo {
  Start-Process powershell.exe -Verb runAs
}

function preview {
  param (
    [string] $url,
    [switch] $keep
  )
  $PREVIEW_DIR = [io.path]::GetTempPath() + ".gitpreview" + [IO.Path]::DirectorySeparatorChar
  if ($keep) {
    $PREVIEW_DIR = $home + [IO.Path]::DirectorySeparatorChar + ".gitPreview" + [IO.Path]::DirectorySeparatorChar
  }
  $CLONE_DIR = $PREVIEW_DIR + [io.path]::GetFileNameWithoutExtension($url)

  if (-NOT (Test-Path -Path $CLONE_DIR)) {
    git clone --depth 1 --single-branch $url $CLONE_DIR
  }
  code -n $CLONE_DIR
}

Set-PSReadLineOption -Colors @{
  "Default"   = [ConsoleColor]::Black
  "Parameter" = [ConsoleColor]::Black
  "Type"      = [ConsoleColor]::DarkBlue
  "Number"    = [ConsoleColor]::Magenta
  "String"    = [ConsoleColor]::Magenta
  "Comment"   = [ConsoleColor]::Yellow
  "Variable"  = [ConsoleColor]::Green
  "Keyword"   = [ConsoleColor]::Blue
  "Operator"  = [ConsoleColor]::Blue
  "Command"   = [ConsoleColor]::Blue
  "Member"    = [ConsoleColor]::Blue
  "Error"     = [ConsoleColor]::Red
}

Function ssh-copy-id {
  param(
    [Parameter(Mandatory = $true)]
    [string] $hostname,
    [string] $key = "~/.ssh/id_rsa.pub"
  )
  Get-Content $key | ssh $hostname 'cat >> ~/.ssh/authorized_keys'
}
$parameters = @{
  Key              = 'ctrl+z'
  BriefDescription = 'SaveInHistory'
  LongDescription  = 'Save current line in history but do not execute'
  ScriptBlock      = {
    param($key, $arg)   # The arguments are ignored in this example

    # GetBufferState gives us the command line (with the cursor position)
    $line = $null
    $cursor = $null
    [Microsoft.PowerShell.PSConsoleReadLine]::GetBufferState([ref]$line,
      [ref]$cursor)

    # AddToHistory saves the line in history, but does not execute it.
    [Microsoft.PowerShell.PSConsoleReadLine]::AddToHistory($line)

    # RevertLine is like pressing Escape.
    [Microsoft.PowerShell.PSConsoleReadLine]::RevertLine()
  }
}
Set-PSReadLineKeyHandler @parameters
$env:BAT_THEME = "OneHalfLight"

Function First {
  [CmdletBinding()]
  Param([Parameter(ValueFromPipeline)] $item)
  $item | select-object -first 1
}

set-alias os out-string

set-alias json ConvertFrom-Json
function to-json {
  [CmdletBinding()]
  Param(
    [Parameter(ValueFromPipeline)]
    $item
  )
  ConvertTo-Json -depth 100 $item
}

function check-lock {
  IF ((Test-Path -Path $args) -eq $false) {
    Write-Warning "File or directory does not exist."
  }
  Else {
    $LockingProcess = CMD /C "openfiles /query /fo table | find /I ""$args"""
    Write-Host $LockingProcess
  }
}


function Invoke-FzfPsReadlineHandlerHistoryExact {
  $result = $null
  try {
    $prevDefaultOpts = $env:FZF_DEFAULT_OPTS
    $env:FZF_DEFAULT_OPTS = $env:FZF_DEFAULT_OPTS + ' ' + $env:FZF_CTRL_R_OPTS

    $line = $null
    $cursor = $null
    [Microsoft.PowerShell.PSConsoleReadline]::GetBufferState([ref]$line, [ref]$cursor)

    $reader = New-Object PSFzf.IO.ReverseLineReader -ArgumentList $((Get-PSReadlineOption).HistorySavePath)

    $fileHist = @{}
    $reader.GetEnumerator() | ForEach-Object {
      if (-not $fileHist.ContainsKey($_)) {
        $fileHist.Add($_, $true)
        $_
      }
    } | Invoke-Fzf -Exact -NoSort -Query "$line" -Bind ctrl-r:toggle-sort | ForEach-Object { $result = $_ }
  }
  catch {
    # catch custom exception
  }
  finally {
    $env:FZF_DEFAULT_OPTS = $prevDefaultOpts
    # ensure that stream is closed:
    $reader.Dispose()
  }

  #HACK: workaround for fact that PSReadLine seems to clear screen
  # after keyboard shortcut action is executed:
  [Microsoft.PowerShell.PSConsoleReadLine]::InvokePrompt()

  if (-not [string]::IsNullOrEmpty($result)) {
    [Microsoft.PowerShell.PSConsoleReadLine]::Replace(0, $line.Length, $result)
  }
}

Set-PSReadLineKeyHandler -Key 'Ctrl+r' -ScriptBlock { Invoke-FzfPsReadlineHandlerHistoryExact }


if ($env:GIT_EDITOR.Length -eq 0) {
  $env:GIT_EDITOR = "`"$($(gcm code).Path -replace "\\","\\")`" --wait"
}

if ($env:NVIM_LISTEN_ADDRESS.Length -gt 0) {
  function Invoke-Neovim {
    nvr -cc split @args
  }
}
else {
  function Invoke-Neovim {
    nvim @args
  }
}

Set-Alias -Name n -Value Invoke-Neovim

function Invoke-Editor {
  $result = $null
  $editor = $env:EDITOR
  if (($null -eq $editor) -or ($editor -eq "")) {
    $editor = "code --wait "
  }
  $line = $null
  $cursor = $null
  try {
    $tempFile = [IO.Path]::GetTempFileName() -replace ".tmp$", ".ps1"

    [Microsoft.PowerShell.PSConsoleReadline]::GetBufferState([ref]$line, [ref]$cursor)
    write-output $line > $tempFile

    invoke-expression "$editor $tempFile"
    $result = Get-Content $tempFile

  }
  catch {
    write-output "error $($_)"
  }
  finally {
    try {
      remove-item $tempFile
    }
    catch { write-output "error $($_)" }
  }

  # HACK: workaround for fact that PSReadLine seems to clear screen
  # after keyboard shortcut action is executed:
  [Microsoft.PowerShell.PSConsoleReadLine]::InvokePrompt()

  if (-not [string]::IsNullOrEmpty($result)) {
    [Microsoft.PowerShell.PSConsoleReadLine]::Replace(0, $line.Length, $result)
  }
}

Set-PSReadLineKeyHandler -Key 'Ctrl+x' -ScriptBlock { Invoke-Editor }

function Setup-Podman {
  podman system connection add wsl --identity C:\Users\saahi\.ssh\id_ed_localhost ssh://saahil@$((wsl hostname -I).Trim())/mnt/wslg/runtime-dir/podman/podman.sock
}

$env:BORE_SECRET = "BORING_TUNNEL"


function Import-Env {
  param(
    [string] $input_filename = ".env"
  )
  if (Test-Path $input_filename) {
    Get-Content $input_filename | ForEach-Object {
      $parts = $_ -Split '=', 2
      if ([string]::IsNullOrEmpty($_)) {

      }
      elseif ($parts.Length -eq 2 ) {
        Write-Host "Setting env:$($parts[0]) from $input_filename"
        Set-Item "env:$($parts[0])" $parts[1]
      }
      else {
        throw "Bad input ($_) - $($parts.Length) $($parts[0]) $($parts[1]) $($parts[2])"
      }
    }
  }
}

Import-Env
if has("win32")
Plug 'autozimu/LanguageClient-neovim', {
            \ 'branch': 'next',
            \ 'do': 'powershell -executionpolicy bypass -File install.ps1',
            \ }
else
Plug 'autozimu/LanguageClient-neovim', {
            \ 'branch': 'next',
            \ 'do': 'bash install.sh',
            \ }
endif
let g:LanguageClient_serverCommands = {
            \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
            \ 'python': ['/home/sclaypool/.local/bin//pyls'],
            \ 'c': ['ccls'],
            \ 'cpp': ['ccls'],
            \ 'objc': ['ccls'],
            \ 'javascript': ['javascript-typescript-stdio'],
            \ }

"Plug 'ncm2/float-preview.nvim'
"set completeopt-=preview
let g:float_preview#docked=0

let g:LanguageClient_useFloatingHover = 1
let g:LanguageClient_useVirtualText = 0
let g:LanguageClient_loadSettings = 1 " Use an absolute configuration path if you want system-wide settings 
let g:LanguageClient_settingsPath = '~/dotfiles/languageclient_settings.json'
set formatexpr=LanguageClient_textDocument_rangeFormatting()
let g:LanguageClient_diagnosticsList="Location"

nnoremap <silent> gh :call <SID>show_documentation()<CR>
function! s:show_documentation()
  if &filetype == 'vim'
    execute 'h '.expand('<cword>')
  else
    call LanguageClient_textDocument_hover()
  endif
endfunction
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> gr :call LanguageClient_textDocument_references()<CR>
nnoremap <silent> gs :call LanguageClient_textDocument_documentSymbol()<CR>
nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>
nnoremap <leader>ld :call LanguageClient#textDocument_definition()<CR>
nnoremap <leader>lr :call LanguageClient#textDocument_rename()<CR>
nnoremap <leader>lf :call LanguageClient#textDocument_formatting()<CR>
nnoremap <leader>lt :call LanguageClient#textDocument_typeDefinition()<CR>
nnoremap <leader>lx :call LanguageClient#textDocument_references()<CR>
nnoremap <leader>la :call LanguageClient_workspace_applyEdit()<CR>
nnoremap <leader>lc :call LanguageClient#textDocument_completion()<CR>
nnoremap <leader>lh :call LanguageClient#textDocument_hover()<CR>
nnoremap <leader>ls :call LanguageClient_textDocument_documentSymbol()<CR>
nnoremap <leader>lm :call LanguageClient_contextMenu()<CR>
nnoremap <leader>lh :call LanguageClient#clearDocumentHighlight()<CR>


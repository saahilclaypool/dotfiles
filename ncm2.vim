" source ~/dotfiles/languageclient.vim
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'

" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()
set completeopt=noinsert,menuone,noselect
" tab to select
" and don't hijack my enter key
inoremap <expr><Tab> (pumvisible()?(empty(v:completed_item)?"\<C-n>":"\<C-y>"):"\<Tab>")
inoremap <silent> <expr> <CR> ncm2_ultisnips#expand_or("\<CR>", 'n')



" NOTE: you need to install completion sources to get completions. Check
" our wiki page for a list of sources: https://github.com/ncm2/ncm2/wiki
Plug 'ncm2/ncm2-bufword'
Plug 'ncm2/ncm2-path'
Plug 'ncm2/ncm2-ultisnips'
Plug 'ncm2/ncm2-tmux'

augroup my_cm_setup
    autocmd!
    autocmd BufEnter * call ncm2#enable_for_buffer()
    autocmd Filetype tex call ncm2#register_source({
                \ 'name': 'vimtex',
                \ 'priority': 8,
                \ 'scope': ['tex'],
                \ 'mark': 'tex',
                \ 'word_pattern': '\w+',
                \ 'complete_pattern': g:vimtex#re#ncm2,
                \ 'on_complete': ['ncm2#on_complete#omni', 'vimtex#complete#omnifunc'],
                \ })
augroup END


let g:ncm2#matcher = 'substrfuzzy'

Plug 'SirVer/ultisnips'
" Snippets are separated from the engine. Add this if you want them:
Plug 'honza/vim-snippets'
let g:UltiSnipsSnippetDir="~/dotfiles/snips"
let g:UltiSnipsSnippetDirectories=[$HOME.'/dotfiles/snips']
let g:UltiSnipsExpandTrigger = "<c-j>"
let g:UltiSnipsListSnippets = "<c-y>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsRemoveSelectModeMappings = 0
let g:UltiSnipsEditSplit="vertical"

" ---------------------------------------------- IDE

Plug 'dense-analysis/ale'
let g:ale_ruby_rubocop_executable='bundle'
nnoremap <silent> ]c :ALENextWrap<CR>
nnoremap <silent> [c :ALEPreviousWrap<CR>
nnoremap gh :ALEDetail<CR>
nnoremap gr :ALEFindReferences<cr>

let g:ale_fixers = {
            \   'ruby': [
            \     'rubocop'
            \],
            \}

let g:ale_linters = {
            \ 'python': ['pylint', 'pyls']
            \}
nnoremap gD :ALEGoToDefinition<cr>

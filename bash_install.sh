git config --global user.name sclaypool
git config --global user.email saahil@hyannisportresearch.com
git clone https://bitbucket.org/saahilclaypool/dotfiles.git /$HOME/dotfiles

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
   https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm


cp /$HOME/dotfiles/.zshrc /$HOME

cp /$HOME/dotfiles/.vimrc /$HOME

cp /$HOME/dotfiles/.tmux.conf /$HOME

sudo apt install neovim tmux ripgrep 

echo "in tmux, use prefix+I and prefix+U to install and update"

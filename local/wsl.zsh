function wvim {
    nvim --listen localhost:1234 --headless $@ +"source ~/dotfiles/ginit.vim"&
    nvim-qt.exe --server localhost:1234 --no-ext-tabline --no-ext-popupmenu --timeout 100
}

alias clip="Clip.exe"

export DISPLAY=$(ip r l default | cut -d' ' -f3):0

alias wget=wget2

alias msedge="/mnt/c/Program\ Files\ \(x86\)/Microsoft/Edge\ Dev/Application/msedge.exe"

alias tmux="tmux -f $HOME/.tmux.conf"


# Named Directories 

export win=/mnt/c/Users/saahi/
export DOTNET_ROOT=$HOME/.dotnet

# start crontab https://github.com/microsoft/WSL/issues/4166#issuecomment-604707989
if ! /etc/init.d/cron status > /dev/null; then
    sudo /etc/init.d/cron start
fi


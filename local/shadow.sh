alias tmux="tmux -f ~/.tmux.conf"
alias hist="history -E"

#[ -d ~/.rbenv/bin ] && export PATH="$HOME/.rbenv/bin:$PATH"
#[ -d ~/.rbenv/bin ] && eval "$(rbenv init -)"

function rbver {
 export RB_VERSION=$(grep "ruby " Gemfile | awk '{print $2}' | sed -e "s/'//g")
 echo $RB_VERSION
}

function bundle {
    [ -f ./.env ] && source .env
    /home/saahil.claypool/.rbenv/shims/bundle $@
}

function rspec {
    [ -f ./.env ] && source .env
    bundle exec rspec $@
}

function rails {
    if [ -f ./Gemfile ]; then
        bundle exec rails $@
    else 
        /home/saahil.claypool/.rbenv/shims/rails $@
    fi 
}

alias rails_ssl="rails s -b \"ssl://127.0.0.1:3000?key=$HOME/.ssh/server.key&cert=$HOME/.ssh/server.crt\""
# k8s stuff

export JUMPCLOUD_USERNAME="saahil.claypool"
export GLOBAL_ACCOUNT_ID="834475807710"
export k8s_VERSION="1.13.2"
export PATH=$HOME/.scripts/bin:$PATH
export PATH=/usr/local/go/bin:$PATH
alias vpn="sudo openvpn --config $HOME/Notes/Config/client.ovpn --auth-user-pass --auth-retry interact"

function rubchanged {
    git diff ${1} --name-only | rg '.rb' | xargs bundle exec rubocop
}

alias rc='rubchanged `gmb`'
alias be='bundle exec'

source $HOME/.cargo/env
export PATH="$PATH:/home/saahil.claypool/.dotnet/tools"


function ,db_dev_scratch {
  dropdb shadowlms_scratch
  createdb shadowlms_scratch 

  pg_dump shadowlms_development > /tmp/scratch.db

  psql -U saahil.claypool shadowlms_scratch < /tmp/scratch.db
  rm /tmp/scratch.db
  vim ./config/database.yml
}

function ,db_mirror_scratch {
  dropdb shadowlms_scratch
  createdb shadowlms_scratch 

  pg_dump dev_mirror > /tmp/scratch.db

  psql -U saahil.claypool shadowlms_scratch < /tmp/scratch.db
  rm /tmp/scratch.db
  vim ./config/database.yml
}

function ,db_bundle_scratch {
  dropdb shadowlms_scratch
  createdb shadowlms_scratch 

  pg_dump shadowlms_bundle > /tmp/scratch.db

  psql -U saahil.claypool shadowlms_scratch < /tmp/scratch.db
  rm /tmp/scratch.db
  vim ./config/database.yml
}

function ,db_current {
  cat ./config/database.yml | rg database: | head -n 1 | awk '{split($0,a," "); print a[2]}'   
}

function ,db_stash {
  dbname=$1
  stashname=$2
  echo $dbname to $stashname

  pg_dump $dbname > ~/.local/$stashname.db
}

function ,db_load {
  dbname=$1
  stashname=$2
  echo $dbname from $stashname
  dropdb $dbname
  createdb $dbname
  psql -U saahil.claypool $dbname < ~/.local/$stashname.db
  vim ./config/database.yml
}

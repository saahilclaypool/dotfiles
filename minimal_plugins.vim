" set termguicolors
"----------------------------------------------------------------------- PLUG

"curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

""
call plug#begin('~/.vim/plugged')

Plug 'tpope/vim-eunuch'

Plug 'editorconfig/editorconfig-vim'
Plug 'lambdalisue/fern.vim'
Plug 'lambdalisue/fern-mapping-project-top.vim' " use ^ for project root
let g:fern#renderer#default#collapsed_symbol = '▷ '
let g:fern#renderer#default#expanded_symbol  = '▼ '
let g:fern#renderer#default#leading          = ' '
let g:fern#renderer#default#leaf_symbol      = ' '
let g:fern#renderer#default#marked_symbol    = '●'
let g:fern#renderer#default#root_symbol      = '~ '
let g:fern#renderer#default#unmarked_symbol  = ''
let g:fern#disable_default_mappings = 1
nnoremap <silent> - :Fern . <CR>
nnoremap <silent> <c-\> :Fern . -drawer -toggle<CR>

function! FernInit() abort
  nmap <buffer><expr>
        \ <Plug>(fern-my-open-expand-collapse)
        \ fern#smart#leaf(
        \   "\<Plug>(fern-action-open:select)",
        \   "\<Plug>(fern-action-expand)",
        \   "\<Plug>(fern-action-collapse)",
        \ )
  nmap <buffer> <CR> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> <space> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> <2-LeftMouse> <Plug>(fern-my-open-expand-collapse)
  nmap <buffer> N <Plug>(fern-action-new-file)
  nmap <buffer> K <Plug>(fern-action-new-dir)
  nmap <buffer> D <Plug>(fern-action-remove)
  nmap <buffer> H <Plug>(fern-action-hidden-toggle)j
  nmap <buffer> R <Plug>(fern-action-reload)
  nmap <buffer> M <Plug>(fern-action-mark-toggle)j
  nmap <buffer> S <Plug>(fern-action-open:split)
  nmap <buffer> V <Plug>(fern-action-open:vsplit)
  nmap <buffer><nowait> < <Plug>(fern-action-leave)
  nmap <buffer><nowait> > <Plug>(fern-action-enter)
  nmap <buffer><nowait> - :bd<cr>
endfunction

augroup FernGroup
  autocmd!
  autocmd FileType fern call FernInit()
augroup END
" --------------------------------------------------------- Themes
Plug 'cormacrelf/vim-colors-github'
Plug 'NLKNguyen/papercolor-theme'


"source ~/dotfiles/coc_config.vim
"source ~/dotfiles/deoplete.vim
"source ~/dotfiles/ncm2.vim
Plug 'ervandew/supertab'

Plug 'christoomey/vim-tmux-navigator'
let g:tmux_navigator_no_mappings = 1
" Disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

nnoremap <silent> <M-h> :TmuxNavigateLeft<cr>
nnoremap <silent> <M-j> :TmuxNavigateDown<cr>
nnoremap <silent> <M-k> :TmuxNavigateUp<cr>
nnoremap <silent> <M-l> :TmuxNavigateRight<cr>

inoremap <silent> <M-h> <c-o>:TmuxNavigateLeft<cr>
inoremap <silent> <M-j> <c-o>:TmuxNavigateDown<cr>
inoremap <silent> <M-k> <c-o>:TmuxNavigateUp<cr>
inoremap <silent> <M-l> <c-o>:TmuxNavigateRight<cr>

tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateLeft<cr>
tnoremap <silent> <M-j> <C-\><C-N>:TmuxNavigateDown<cr>
tnoremap <silent> <M-k> <C-\><C-N>:TmuxNavigateUp<cr>
tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateRight<cr>


Plug 'junegunn/goyo.vim'
let g:goyo_width = '120' 
let g:goyo_height = '100%'
let g:goyo_linenr = 0
nnoremap <silent> <C-w>z :Goyo 100%x100%<cr>
nnoremap <silent> <C-w><C-z> :Goyo!<cr>



Plug 'kassio/neoterm'
xmap <Leader>, <Plug>(neoterm-repl-send)
nmap <Leader>, <Plug>(neoterm-repl-send-line)
let g:neoterm_size = 10
let g:neoterm_autoinsert = 1
let g:neoterm_default_mod = 'rightbelow'
map <silent> <C-space> :Ttoggle<cr>
map <silent> <leader><C-space> :terminal<cr>i



Plug 'vim-airline/vim-airline'
let g:airline_extensions = ['branch']
Plug 'vim-airline/vim-airline-themes'
let g:airline#extensions#tabline#enabled = 0
let g:airline_detect_spell=1
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#branch#format = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'


Plug 'markonm/traces.vim'


set signcolumn=yes
Plug 'Shougo/echodoc.vim'
let g:echodoc#enable_at_startup = 1
let g:echodoc#type = 'echo'

" GIT ====================================================
Plug 'tpope/vim-fugitive'
nnoremap <leader>gg :Git<CR>
nnoremap <leader>gl :Gclog! -100<CR>
nnoremap <leader>gd :Gvdiffsplit <Space>
nnoremap <leader>GD :Gdiffsplit!<CR>
nnoremap <leader>gb :Git blame<CR>
nnoremap <leader>ge :Gvdiffsplit origin/master:%<Space>

Plug 'airblade/vim-gitgutter'
let g:gitgutter_map_keys = 0
nmap ]g <Plug>(GitGutterNextHunk)
nmap [g <Plug>(GitGutterPrevHunk)
nnoremap <leader>gs :GitGutterStageHunk<CR>
nnoremap <leader>gu :GitGutterUndoHunk<CR>
" TOGGLE --------------------------------------------
nnoremap <silent> gp :call ToggleGitGutterPreviewHunk()<CR>
function! ToggleGitGutterPreviewHunk() abort
    " does nothing if that command doesn't exist
    if !exists(':GitGutterPreviewHunk')
        return 0
    endif

    " loop through all the windows in the current tab page
    for win in range(1, winnr('$'))

        " is it a preview window?
        let preview_window = getwinvar(win, '&previewwindow') ? win : 0
    endfor

    " we have a preview window
    if preview_window > 0

        " close the preview window
        pclose

        " we don't have a preview window
    else

        " open the preview window
        GitGutterPreviewHunk
    endif
endfunction

Plug 'tpope/vim-dispatch'

" VIM basics ----------------------------------------------
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
let g:NERDAltDelims_c = 1
map <silent> \ <leader>c<leader>
map <silent> <leader>/ <leader>c<leader>
let g:NERDCustomDelimiters = {
            \ 'test': { 'left': '//'},
            \ }

Plug 'junegunn/vim-easy-align'
vnoremap <silent> a :LiveEasyAlign<CR>

Plug 'justinmk/vim-sneak'
let g:sneak#label = 1
let g:sneak#use_ic_scs = 1
nnoremap <silent> f :<C-U>call sneak#wrap('',           1, 0, 1, 1)<CR>
nnoremap <silent> F :<C-U>call sneak#wrap('',           1, 1, 1, 1)<CR>
xnoremap <silent> f :<C-U>call sneak#wrap(visualmode(), 1, 0, 1, 1)<CR>
xnoremap <silent> F :<C-U>call sneak#wrap(visualmode(), 1, 1, 1, 1)<CR>
onoremap <silent> f :<C-U>call sneak#wrap(v:operator,   1, 0, 1, 1)<CR>
onoremap <silent> F :<C-U>call sneak#wrap(v:operator,   1, 1, 1, 1)<CR>
Plug 'easymotion/vim-easymotion'
map  <Leader>s <Plug>(easymotion-bd-f)


Plug 'michaeljsmith/vim-indent-object'
Plug 'mbbill/undotree'
map <Leader>u :UndotreeToggle<cr>:UndotreeFocus<cr>
Plug 'raimondi/delimitMate'
let delimitMate_expand_cr = 2
let g:delimitMate_nesting_quotes = ['"','`']

" Search --------------------------------------------------
let g:ackhighlight = 1
let g:ack_apply_lmappings = 0
let g:ack_use_dispatch = 1

Plug 'airblade/vim-rooter'
let g:rooter_patterns = ['Cargo.toml', 'Rakefile', '.git/', '.root']
let g:rooter_manual_only = 0
nnoremap <Leader>CD :Rooter<CR>:source .vimrc<CR>:pwd<CR>

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

nnoremap <C-p> :FZF<CR>
nnoremap <Leader>b :Buffers<CR>
nnoremap <leader><tab> :Buffers<CR>
"nnoremap <Leader>s :BLines<CR>
nnoremap <Leader>S :Rg<space>
nnoremap <Leader><Leader>s :Lines<CR>
nnoremap <Leader>H :Helptags<CR>
nnoremap <Leader>h :History<CR>
nnoremap <leader>;  :<up>
let g:fzf_buffers_jump = 0
let g:fzf_tags_command = 'ctags -R --sort=yes --excmd=number --tag-relative=yes --machinable=yes'
nnoremap <silent> <Leader>t :BTags<CR>
nnoremap <silent> <Leader>T :Tags<CR>

let g:USE_IGNORE = 1
let g:_FZF_DEFAULT_COMMAND = "rg --files --hidden --glob \"!.git/*\" --no-messages "

if has("nvim")
    au TermOpen * tnoremap <Esc> <c-\><c-n>
    au TermOpen * tnoremap <c-w><Esc><Esc> <Esc>
    au TermOpen * tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateLeft<cr>
    au TermOpen * tnoremap <silent> <M-j> <C-\><C-N>:TmuxNavigateDown<cr>
    au TermOpen * tnoremap <silent> <M-k> <C-\><C-N>:TmuxNavigateUp<cr>
    au TermOpen * tnoremap <silent> <M-l> <C-\><C-N>:TmuxNavigateRight<cr>
    au BufEnter * if &buftype == 'terminal' | :startinsert | endif
    autocmd! FileType fzf
    "autocmd  FileType fzf set laststatus=0 noshowmode noruler
    "\| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
    au FileType fzf tunmap <Esc>
    " FZF in a floating window
    "let $FZF_DEFAULT_OPTS.='--layout=reverse'
    let g:fzf_layout = { 'window': 'call FloatingFZF()' }

    function! FloatingFZF()
        let buf = nvim_create_buf(v:false, v:true)
        call setbufvar(buf, '&signcolumn', 'no')

        "let height = &lines - 5
        let height = &lines / 2
        let width = min([(&columns * 7 / 10), 120])

        "let col = float2nr((&columns - width) / 2)
        let col = float2nr(0)
        let row = float2nr(&lines / 2)
        "let row = 5

                    "\ 'row': &lines / 2,
                    "\ 'col': &columns - &width / 2,
                    "\ 'relative': 'win',
        let opts = {
                    \ 'relative': 'win',
                    \ 'row': row,
                    \ 'col': col,
                    \ 'width': width,
                    \ 'height': height,
                    \ 'style': 'minimal'
                    \ }

        call nvim_open_win(buf, v:true, opts)
    endfunction
endif

" When using `dd` in the quickfix list, remove the item from the quickfix list.
" cdo will apply to all lines in the quickfix window
function! RemoveQFItem()
    let curqfidx = line('.') - 1
    let qfall = getqflist()
    call remove(qfall, curqfidx)
    call setqflist(qfall, 'r')
    execute curqfidx + 1 . "cfirst"
    :copen
endfunction
:command! RemoveQFItem :call RemoveQFItem()
" Use map <buffer> to only map dd in the quickfix window. Requires +localmap
autocmd FileType qf map <buffer> dd :RemoveQFItem<cr>

" Syntax --------------------------------------------------

Plug 'sheerun/vim-polyglot'
let g:jsx_ext_required = 1
let g:polyglot_disabled = ['latex', 'tex', 'markdown', 'csv']
let g:tex_flavor = "latex"
Plug 'lervag/vimtex'
Plug 'jceb/vim-orgmode'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'

let g:vim_markdown_override_foldtext = 0
let g:vim_markdown_no_default_key_mappings = 1
let g:vim_markdown_folding_level = 10
let g:vim_markdown_folding_disabled = 0
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_auto_insert_bullets = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_folding_style_pythonic = 1


call plug#end()

" Completion--------------------------------------------------

set complete-=i
set pumheight=7

" THEME --------------------------------------------------


let g:airline_section_a =     airline#section#create(['mode', 'paste', 'readonly'])
let g:airline_section_b =     airline#section#create([''])
let g:airline_section_c =     airline#section#create(['file'])
let g:airline_section_y =     airline#section#create(['filetype'])
let g:airline_section_x =     airline#section#create(['%-0.30{fnamemodify(getcwd(), ":~:.")}', '  %{airline#util#wrap(airline#extensions#branch#get_head(), 80)}'])
let g:airline_section_z = airline#section#create(['%5l', '/%-5L%', '::%3v', '  %3p%%'])
let g:airline_section_error = airline#section#create([''])
let g:airline_section_warning = airline#section#create([''])
let g:airline_theme='sol'
"let g:airline_theme='seagull'
let g:airline_mode_map = {
            \ '__'     : '-',
            \ 'c'      : 'C',
            \ 'i'      : 'I',
            \ 'ic'     : 'I',
            \ 'ix'     : 'I',
            \ 'n'      : 'N',
            \ 'multi'  : 'M',
            \ 'ni'     : 'N',
            \ 'no'     : 'N',
            \ 'R'      : 'R',
            \ 'Rv'     : 'R',
            \ 's'      : 'S',
            \ 'S'      : 'S',
            \ ''     : 'S',
            \ 't'      : 'T',
            \ 'v'      : 'V',
            \ 'V'      : 'V',
            \ ''     : 'V',
            \ }

let g:PaperColor_Theme_Options = {
            \   'language': {
            \     'c': {
            \       'highlight_builtins' : 1
            \     },
            \    },
            \   'theme': {
            \     'default.light': {
            \       'transparent_background': 1,
            \       'allow_bold': 1,
            \       'allow_italic': 1,
            \       'override' : {
            \          'color00' : ['#fdf6e3', '256'],
            \          'color07' : ['#000000', '256'],
            \          'cursorline': ['#fdf6e3', '255'],
            \          'cursor_fg': ['#cc0000', '255'],
            \          'cursor_bg': ['#ffffff', '255'],
            \       }
            \     }
            \   }
            \ }
set background=light " for the light version
colorscheme github

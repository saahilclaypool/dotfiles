mkdir ~/Software
git clone https://github.com/faho/kwin-tiling.git ~/Software/kwin-tiling
cd ~/Software/kwin-tiling
plasmapkg2 --type kwinscript -i .

mkdir -p ~/.local/share/kservices5
ln -s ~/.local/share/kwin/scripts/kwin-script-tiling/metadata.desktop ~/.local/share/kservices5/kwin-script-tiling.desktop
    

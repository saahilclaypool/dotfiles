Plug 'dense-analysis/ale'
let g:ale_ruby_rubocop_executable='bundle'


let g:ale_fixers = {
            \   'ruby': [
            \       'rubocop'
            \   ],
            \   'python': [
            \       'autopep8'
            \   ],
            \}

let g:ale_linters = {
            \ 'python': ['pylint', 'pyls']
            \}

nnoremap ]a :ALENextWrap<cr>
nnoremap [a :ALEPreviousWrap<cr>
nnoremap ga :ALEDetail<cr>
nnoremap =a :ALEFix<cr>

